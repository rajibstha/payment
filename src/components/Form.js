import React, { Component } from 'react'
import './Form.css';

export default class Form extends Component {
    render() {
        return (
            <div className="form">
                <form>
                    <div className="name">
                        <span>Full Name</span>
                        <input type="text" placeholder="Full Name" pattern  maxLength="25" />
                    </div>
                    <div className="number">
                        <span>Card Number</span>
                        <input id="input-cc" 
                        name="credit-number" 
                        class="cc-number" 
                        type="tel" 
                        pattern="\d*" 
                        maxlength="16" 
                        placeholder="Card Number" />
                    </div>
                    <div className="date">
                        <span>Expiry date</span>
                        <input type ="date" />
                    </div>
                    <div className="code">
                        <span>CVC Code</span>
                        <input type="tel" placeholder="XXX" maxLength="3" />
                    </div>
                </form>
            </div>
        )
    }
}
