import React, { Component } from 'react'
import './Counter.css';
export default class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicks: 0,
      show: true
    };
  }

  IncrementItem = () => {
    this.setState({ clicks: this.state.clicks + 1 });
  }
  DecreaseItem = () => {
    this.setState({ clicks: this.state.clicks - 1 });
  }
  ToggleClick = () => {
    this.setState({ show: !this.state.show });
  }

  render() {
    return (
      <div className="counter">
        <button onClick={this.DecreaseItem}>-</button>
        { this.state.show ? <h4>{ this.state.clicks }</h4> : '' }
        <button onClick={this.IncrementItem}>+</button> 
      </div>
    );
  }
}

