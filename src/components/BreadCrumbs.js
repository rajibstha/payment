import React, { Component } from 'react'
import './BreadCrumbs.css';
export default class BreadCrumbs extends Component {
    render() {
        return (
            <div>
                <ul className="breadcrumbs">
                    <li className="breadcrumbs__item">
                        <a href="/" className="breadcrumbs__link">Home</a>
                    </li>
                    <li className="breadcrumbs__item">
                      <span>Payment</span>
                    </li>
                </ul>
            </div>
        )
    }
}
