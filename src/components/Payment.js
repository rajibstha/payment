import React, { Component } from 'react'
import './Payment.css'
import Visa from '../IconComponents/Visa';
import Mastercard from '../IconComponents/Mastercard';
import Americanexpress from '../IconComponents/Americanexpress';
import Shoes from '../IconComponents/Shoes';
import Sunglasses from '../IconComponents/Sunglasses';
import Watch from '../IconComponents/Watch';
import Counter from './Counter';

import BreadCrumbs from './BreadCrumbs';
import Form from './Form';


export default class Payment extends Component {
    render() {
        return (
            <div id="wrapper">  
                <BreadCrumbs />  
                <div className="payment">
                    <div className="browse-items">
                        <div className="items">
                            <h3>Items</h3>
                            <div className="shoes">
                                <div className="touch-icon">
                                    <Shoes />
                                </div>
                                <div className="content">
                                    <div className="items-name">
                                        <span>Addidas</span> 
                                        <span>No.1 shoes</span>
                                        <span>Quantity</span>
                                    </div>
                                    <div className="counter">
                                    <Counter /> 
                                    </div>
                                    <div className="quantity">
                                        <span>{this.props.text}</span>
                                    </div>
                                </div>
                            </div>
                            <div className="watch">
                                <div className="touch-icon">
                                    <Watch />
                                </div>
                                <div className="content">
                                    <div className="items-name">
                                        <span>Diesel</span>
                                        <span>DZ2XJIG</span>
                                        <span>20mm</span>
                                    </div>
                                    <div className="counter">
                                        <Counter />
                                    </div>
                                </div>
                            </div>
                            <div className="Glasses">
                                <div className="touch-icon">
                                    <Sunglasses />
                                </div>
                                <div className="content">
                                    <div className="items-name">
                                        <span>Rayban</span>
                                        <span>Glasses</span>
                                        <span>GTX</span>
                                    </div>
                                    <div className="counter">
                                        <Counter />
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div className="payment-option">
                        <h3>Card Details</h3>
                        <div className="cards">
                            <div className="touch-icon1">
                                <Mastercard />
                            </div>
                            <div className="touch-icon1">
                                <Visa />    
                            </div>
                            <div className="touch-icon1">
                                <Americanexpress />      
                            </div>
                        </div>       
                        <Form />                           
                        <div className="submit">
                            <a href="/"><span>Confirm</span></a>
                        </div>
                    
                    </div>
                </div>
            </div>
        )
    }
}